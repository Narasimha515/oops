
import java.util.Scanner;

public abstract class P {

	    int a, b, sum;

	    abstract void input();
	    abstract void add();
	    abstract void result();
	    Scanner sc = new Scanner(System.in);
	}

public class Q extends class P{

	    void input() {
	        System.out.print("Enter Two Numbers :");
	        a = sc.nextInt();
	        b = sc.nextInt();
	    }

	    void add() {
	        sum = a + b;
	    }

	    void result() {
	        System.out.print("The Sum of two numbers is :" + sum);
	    }

	    public static void main(String args[]) {
	        Q q = new Q();
	        q.input();
	        q.add();
	        q.result();
	    }
	}

